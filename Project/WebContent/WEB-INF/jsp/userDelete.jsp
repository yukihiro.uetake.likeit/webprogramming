<%@page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>title</title>
</head>

<body>
	<nav class="navbar navbar-light" style="background-color: #e3f2fd;">

		<span style="" class="navbar-text"> ${userInfo.name}さん </span> <span style=""
			class="navbar navbar-expand-lg navbar-light bg-light"> <a
			class="navbar-brand" href="#">ログアウト</a>
		</span>
	</nav>
	<h1 style="text-align: center">ユーザ情報更新</h1>

	<form class="form-signin" action="UserDeleteServlet" method="post">
		<div style="text-align: center">
			<h>ログインID:</h>
			<h style="text-align:center">${user.loginId}</h>

		</div>

		<div style="text-align: center">
			<h>を本当に削除してよろしいでしょうか。</h>

		</div>
		<input type="hidden" name="id" value="${user.id}">
		<div style="text-align: center">
			<button class="btn btn-lg btn-primary btn-block btn-signin"
				type="submit">キャンセル</button>
			<button class="btn btn-lg btn-primary btn-block btn-signin"
				type="submit">OK</button>

		</div>
	</form>

	<a class="navbar-brand" href="UserListServlet">戻る</a>

</body>

</html>