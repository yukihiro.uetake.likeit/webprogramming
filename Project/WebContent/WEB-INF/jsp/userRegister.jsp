<%@page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>title</title>
</head>

<body>
	<nav class="navbar navbar-light" style="background-color: #e3f2fd;">

		<span style="" class="navbar-text"> ${userInfo.name}さん </span> <span style=""
			class="navbar navbar-expand-lg navbar-light bg-light"> <a
			class="navbar-brand" href="LogoutServlet">ログアウト</a>
		</span>
	</nav>
	<h1 style="text-align: center">ユーザ新規登録</h1>

	<c:if test="${errMsg != null}">
		<div style="text-align: center ;color:red" class="alert alert-danger" role="alert" >${errMsg}</div>
	</c:if>

	<form class="form-signin" action="UserRegisterServlet" method="post">

		<div style="text-align: center">
			<h>ログインID</h>
			<input type="text" name="loginid" id="inputLoginId"
				class="form-control">
		</div>

		<div style="text-align: center">
			<h>パスワード</h>
			<input type="text" name="password" id="inputLoginId"
				class="form-control">
		</div>

		<div style="text-align: center">
			<h>パスワード確認</h>
			<input type="text" name="password1" id="inputLoginId"
				class="form-control">
		</div>

		<div style="text-align: center">
			<h>ユーザ名</h>
			<input type="text" name="name" id="inputLoginId" class="form-control">
		</div>

		<div style="text-align: center">
			<h>生年月日</h>
			<input type="text" name="birthdate" id="inputLoginId"
				class="form-control">
		</div>

		<div style="text-align: center">
			<button class="btn btn-lg btn-primary btn-block btn-signin"
				type="submit">登録</button>
		</div>
	</form>

	<a class="navbar-brand" href="UserListServlet">戻る</a>

</body>

</html>
