<%@page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset = "utf-8">
    <title>title</title>
    </head>

    <body>
        <nav class="navbar navbar-light" style="background-color: #e3f2fd;">

  <span style=center class="navbar-text">
    ${userInfo.name}さん
  </span>

  <span style=right  class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">ログアウト</a>
</span>
  </nav>
         <h1 style="text-align:center">ユーザ情報詳細参照</h1>

        <div>
            <h style="text-align:left">ログインID</h>
            <h style="text-align:center">${requestScope.user.loginId}</h>
        <div>
        <h style="text-align:left">ユーザ名</h>
            <h style="text-align:center">${requestScope.user.name}</h>
        <div>
            <h style="text-align:left">生年月日</h>
            <h style="text-align:center">${requestScope.user.birthDate}</h>
        <div>
            <h style="text-align:left">登録日時</h>
            <h style="text-align:center">${requestScope.user.createDate}</h>
        <div>
            <h style="text-align:left">更新日時</h>
            <h style="text-align:center">${requestScope.user.updateDate}</h>
        <div>
  <a class="navbar-brand" href="UserListServlet">戻る</a>

    </body>

    </html>
