<%@page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>title</title>
</head>

<body>
	<nav class="navbar navbar-light" style="background-color: #e3f2fd;">

		<span style="" class="navbar-text"> ${userInfo.name}さん </span> <span style=""
			class="navbar navbar-expand-lg navbar-light bg-light"> <a
			class="navbar-brand" href="LogoutServlet">ログアウト</a>
		</span>
	</nav>
	<h1 style="text-align: center">ユーザ情報更新</h1>

	<c:if test="${errMsg != null}">
		<div style="text-align: center ;color:red" class="alert alert-danger" role="alert" >${errMsg}</div>
	</c:if>

	<form class="form-signin" action="UserUpdateServlet" method="post">
		<div style="text-align: center">
			<h>ログインID</h>
			<h style="text-align:center">${requestScope.user.loginId}</h>
		</div>
		</div>

		<div style="text-align: center">
			<h>パスワード</h>
			<input type="text" name="password" id="inputLoginId"
				class="form-control" >
		</div>

		<div style="text-align: center">
			<h>パスワード確認</h>
			<input type="text" name="password1" id="inputLoginId"
				class="form-control" >
		</div>

		<div style="text-align: center">
			<h>ユーザ名</h>
			<input type="text" value="${requestScope.user.name}" name="name"
				id="inputLoginId" class="form-control" >
		</div>

		<div style="text-align: center">
			<h>生年月日</h>
			<input type="text" value="${requestScope.user.birthDate}"
				name="birthdate" id="inputLoginId" class="form-control" >
		</div>
	<input type="hidden"  name="id" value="${user.id}">
		<div style="text-align: center">
			<button class="btn btn-lg btn-primary btn-block btn-signin"
				type="submit">更新</button>
		</div>
	</form>

	<a class="navbar-brand" href="UserListServlet">戻る</a>

</body>

</html>
